[![pipeline status](https://gitlab.com/poilov.mv/otus-template-method/badges/master/pipeline.svg)](https://gitlab.com/poilov.mv/otus-template-method/-/commits/master)
[![coverage report](https://gitlab.com/poilov.mv/otus-template-method/badges/master/coverage.svg)](https://gitlab.com/poilov.mv/otus-template-method/-/commits/master)

# Шаблоны проектирования

## Домашнее задание по теме **Шаблонный метод**
Реализация выбора подходящего метода матричных операций с применением шаблонного метода и описание применения шаблона в проекте
**Цель**: Получите навыки в программировании алгоритмов матричных операций, применении шаблонного метода.
Есть несколько операций над матрицами:
1. транспонирование матрицы
2. сложение матриц
3. найти определитель матрицы

Написать программу, которая выполняет следующее:
0. на входе получает название входного файла, выходного файла и вид операции
1. Получает данные из файла
2. Выполняет указанную операцию над данными
3. Формирует данные для вывода в необходимом формате
4. Записывает данные в выходной файл
5. Если потребуется использовать Шаблонный метод в проектной работе, предоставить описание в текстовом файле в GitHub репозитории где конкретно и в какой роли используется этот шаблон.
6. нарисовать диаграмму классов.

## Диаграмма классов
### Наследование
```mermaid
classDiagram
    class TransposeMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class SumMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class DeterminantMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class IMatrixFileProcessorFactory {
       +create() AbstractMatrixFileProcessor
       +operation() string
     }

    TransposeMatrixFileProcessorFactory --|> IMatrixFileProcessorFactory
    SumMatrixFileProcessorFactory --|> IMatrixFileProcessorFactory
    DeterminantMatrixFileProcessorFactory --|> IMatrixFileProcessorFactory

    AbstractMatrixFileProcessor <|-- TransposeMatrixFileProcessor
    AbstractMatrixFileProcessor <|-- SumMatrixFileProcessor
    AbstractMatrixFileProcessor <|-- DeterminantMatrixFileProcessor

    IMatrixFileProcessorFactory ..> AbstractMatrixFileProcessor : использует

    class AbstractMatrixFileProcessor {
      +apply(sourceFilePath, targetFilePath)
      -readFile(sourceFilePath) : string
      -writeFile(content, targetFilePath)
      #applyOp(content) : string
    }
    class TransposeMatrixFileProcessor {
      #applyOp(content) : string
    }
    class SumMatrixFileProcessor {
      #applyOp(content) : string
    }
    class DeterminantMatrixFileProcessor {
      #applyOp(content) : string
    }
```
### Создание
```mermaid
classDiagram
    class TransposeMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class SumMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class DeterminantMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }

    TransposeMatrixFileProcessorFactory --> TransposeMatrixFileProcessor : создает
    SumMatrixFileProcessorFactory --> SumMatrixFileProcessor : создает
    DeterminantMatrixFileProcessorFactory --> DeterminantMatrixFileProcessor : создает

    class TransposeMatrixFileProcessor {
      #applyOp(content) : string
    }
    class SumMatrixFileProcessor {
      #applyOp(content) : string
    }
    class DeterminantMatrixFileProcessor {
      #applyOp(content) : string
    }
```
### Вспомогательные классы
```mermaid
classDiagram
  IMatrixFileProcessorFactory <.. MatrixOperations : list<AbstractMatrixFileProcessor> items

  class MatrixOperations {
    +create(operation) AbstractMatrixFileProcessor
    +operations() strings
  }

  MatrixOperations *-- TransposeMatrixFileProcessorFactory
  MatrixOperations *-- SumMatrixFileProcessorFactory
  MatrixOperations *-- DeterminantMatrixFileProcessorFactory

  class ArgumentParser {
    list<string> args
    +isArgsValid() bool
    +operation() string
    +sourceFilePath() string
    +targetFilePath() string
    +synopsis() string
  }
```
```mermaid
  classDiagram

  class TextFile {
    string filepath
    string content

    +TextFile(filepath)
    +path() string
    +setContent(value)
    +getContent()
    +read() bool
    +write() bool
    +exists() bool
    +remove()
  }

  class Matrix3 {
    COLS static const int = 3
    ROWS static const int = 3

    +Matrix3()
    +fromVector(value) Matrix3 static
    +toVector() vector<double>
    +at(col, row) double
    +transposed() Matrix3
    +operator+(other) Matrix3
    +det() double
  }

  class Matrix3StringFormatter {
    +toString(matrix) string
    +fromString(value, out matrix) bool

    +toString(matrix1, matrix2) string
    +fromString(value, out matrix1, out matrix2) bool
  }
```

## Исходные файлы
### Основное приложение
* source/main.cpp
* source/main_app.h
* source/main_app.cpp
### Интерфейсы
* source/IMatrixFileProcessorFactory.h
### Абстрактные классы
* source/AbstractMatrixFileProcessor.h
* source/AbstractMatrixFileProcessor.cpp
### Реализация операций над матрицами
* source/TransposeMatrixFileProcessor.h
* source/TransposeMatrixFileProcessor.cpp
* source/SumMatrixFileProcessor.h
* source/SumMatrixFileProcessor.cpp
* source/DeterminantMatrixFileProcessor.h
* source/DeterminantMatrixFileProcessor.cpp
### Реализация фабрик операций над матрицами
* source/TransposeMatrixFileProcessorFactory.h
* source/TransposeMatrixFileProcessorFactory.cpp
* source/SumMatrixFileProcessorFactory.h
* source/SumMatrixFileProcessorFactory.cpp
* source/DeterminantMatrixFileProcessorFactory.h
* source/DeterminantMatrixFileProcessorFactory.cpp
### Класс разбора аргументов командной строки
* source/ArgumentParser.h
* source/ArgumentParser.cpp
### Класс агрегатор списка фабрик операций над матрицами
* source/MatrixOperations.h
* source/MatrixOperations.cpp
### Класс обертка для чтения и сохранения текстового файла
* source/TextFile.h
* source/TextFile.cpp
### Класс матрицы 3х3
* source/Matrix3.h
* source/Matrix3.cpp
### Класс форматировщик в строку матрицы 3х3
* source/Matrix3StringFormatter.h
* source/Matrix3StringFormatter.cpp
### Тесты
* unit_tests/test_abstractmatrixfileprocessor.cpp
* unit_tests/test_argumentparser.cpp
* unit_tests/test_determinantmatrixfileprocessor.cpp
* unit_tests/test_determinantmatrixfileprocessorfactory.cpp
* unit_tests/test_main.cpp
* unit_tests/test_matrix3.cpp
* unit_tests/test_matrix3stringformatter.cpp
* unit_tests/test_matrixoperations.cpp
* unit_tests/test_summatrixfileprocessor.cpp
* unit_tests/test_summatrixfileprocessorfactory.cpp
* unit_tests/test_textfile.cpp
* unit_tests/test_transposematrixfileprocessor.cpp
* unit_tests/test_transposematrixfileprocessorfactory.cpp

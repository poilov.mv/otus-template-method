#pragma once

#include <fstream>
#include <sstream>


class TextFile {
    std::string filepath;
    std::string content;

public:
    TextFile(const std::string &filepath);

    std::string path() const;

    void setContent(const std::string &value);
    std::string getContent() const;

    bool read();
    bool write();

    bool exists() const;

    void remove();
};

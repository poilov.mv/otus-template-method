#pragma once

#include "AbstractMatrixFileProcessor.h"


class TransposeMatrixFileProcessor
    : public AbstractMatrixFileProcessor {
protected:
    std::string applyOp(const std::string &content) override;
};

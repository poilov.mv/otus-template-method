#include "Matrix3StringFormatter.h"

#include <sstream>


std::string Matrix3StringFormatter::toString(const Matrix3 &matrix) {
    auto v = matrix.toVector();
    std::stringstream ss;
    for (int i = 0; i < v.size(); ++i) {
        if (i != 0) {
            ss << " ";
        }
        ss << v[i];
    }
    return ss.str();
}

bool Matrix3StringFormatter::fromString(const std::string& value, Matrix3 *matrix) {
    std::istringstream ss(value);
    for (int i = 0; i<Matrix3::ROWS; ++i) {
        for (int j = 0; j<Matrix3::COLS; ++j) {
            ss >> matrix->at(j, i);
        }
    }
    return ss.eof() && !ss.fail();
}

std::string Matrix3StringFormatter::toString(const Matrix3 &matrix1, const Matrix3 &matrix2) {
    return toString(matrix1) + "\n" + toString(matrix2);
}

bool Matrix3StringFormatter::fromString(const std::string& value, Matrix3 *matrix1, Matrix3 *matrix2) {
    std::istringstream ss(value);
    
    std::string line1;
    if (!std::getline(ss, line1)) {
        return false;
    }
    
    std::string line2;
    if (!std::getline(ss, line2)) {
        return false;
    }

    return fromString(line1, matrix1) && fromString(line2, matrix2);
}

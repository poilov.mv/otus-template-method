#include "SumMatrixFileProcessorFactory.h"
#include "SumMatrixFileProcessor.h"


std::unique_ptr<AbstractMatrixFileProcessor> SumMatrixFileProcessorFactory::create() {
    return std::make_unique<SumMatrixFileProcessor>();
}

std::string SumMatrixFileProcessorFactory::operation() const {
    return "sum";
}

#include "Matrix3.h"


Matrix3::Matrix3()
    : m(COLS * ROWS) {}

Matrix3 Matrix3::fromVector(const std::vector<double> &value) {
    Matrix3 matrix;
    matrix.m = value;
    return matrix;
}

std::vector<double> Matrix3::toVector() const {
    return m;
}

double &Matrix3::at(int col, int row) {
    return m.at(col + row * COLS);
}

const double &Matrix3::at(int col, int row) const {
    return m.at(col + row * COLS);
}

Matrix3 Matrix3::transposed() const {
    Matrix3 result;
    for (int i = 0; i<Matrix3::ROWS; ++i) {
        for (int j = 0; j<Matrix3::COLS; ++j) {
            result.at(i, j) = at(j, i);
        }
    }
    return result;
}

Matrix3 Matrix3::operator+ (const Matrix3 &other) const {
    Matrix3 result = fromVector(m);
    for (int i = 0; i<Matrix3::ROWS; ++i) {
        for (int j = 0; j<Matrix3::COLS; ++j) {
            result.at(j, i) += other.at(j, i);
        }
    }
    return result;
}

double Matrix3::det() const {
    return at(0, 0) * at(1, 1) * at(2, 2) - at(0, 0) * at(2, 1) * at(1, 2)
        - at(1, 0) * at(0, 1) * at(2, 2) + at(1, 0) * at(2, 1) * at(0, 2)
        + at(2, 0) * at(0, 1) * at(1, 2) - at(2, 0) * at(1, 1) * at(0, 2);
}

#include "DeterminantMatrixFileProcessor.h"

#include "Matrix3StringFormatter.h"
#include <sstream>


std::string DeterminantMatrixFileProcessor::applyOp(const std::string &content) {
    Matrix3 matrix;
    if (!Matrix3StringFormatter::fromString(content, &matrix)) {
        throw MatrixFileProcessorError("Wrong file format");
    }

    auto determinant = matrix.det();

    std::stringstream ss;
    ss << determinant;
    return ss.str();
}

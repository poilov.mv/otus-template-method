#pragma once

#include "IMatrixFileProcessorFactory.h"


class SumMatrixFileProcessorFactory
    : public IMatrixFileProcessorFactory {
public:
    std::unique_ptr<AbstractMatrixFileProcessor> create() override;

    std::string operation() const override;
};

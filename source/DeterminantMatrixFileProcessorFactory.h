#pragma once

#include "IMatrixFileProcessorFactory.h"


class DeterminantMatrixFileProcessorFactory
    : public IMatrixFileProcessorFactory {
public:
    std::unique_ptr<AbstractMatrixFileProcessor> create() override;

    std::string operation() const override;
};

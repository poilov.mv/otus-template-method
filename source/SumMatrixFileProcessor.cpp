#include "SumMatrixFileProcessor.h"

#include "Matrix3StringFormatter.h"


std::string SumMatrixFileProcessor::applyOp(const std::string &content) {
    Matrix3 matrix1;
    Matrix3 matrix2;
    if (!Matrix3StringFormatter::fromString(content, &matrix1, &matrix2)) {
        throw MatrixFileProcessorError("Wrong file format");
    }

    matrix2 = matrix1 + matrix2;

    return Matrix3StringFormatter::toString(matrix2);
}

#include "ArgumentParser.h"

#include <sstream>


ArgumentParser::ArgumentParser(int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        args.push_back(argv[i]);
    }
}

bool ArgumentParser::isArgsValid() const {
    return args.size() == 3;
}

std::string ArgumentParser::synopsis(const std::vector<std::string> &operations) const {
    std::stringstream ss;
    ss << "Usage:" << std::endl;

    for (auto const& type : operations) {
        ss << "sortfile.exe " << type << " <input_file> <output_file>" << std::endl;
    }

    return ss.str();
}

std::string ArgumentParser::operation() const {
    return args[0];
}

std::string ArgumentParser::sourceFilePath() const {
    return args[1];
}

std::string ArgumentParser::targetFilePath() const {
    return args[2];
}

#include "MatrixOperations.h"

#include "TransposeMatrixFileProcessorFactory.h"
#include "SumMatrixFileProcessorFactory.h"
#include "DeterminantMatrixFileProcessorFactory.h"

#include <algorithm>
#include <cctype>


MatrixOperations::MatrixOperations() {
    items.emplace_back(std::make_unique<TransposeMatrixFileProcessorFactory>());
    items.emplace_back(std::make_unique<SumMatrixFileProcessorFactory>());
    items.emplace_back(std::make_unique<DeterminantMatrixFileProcessorFactory>());
}

std::vector<std::string> MatrixOperations::operations() const {
    std::vector<std::string> result;
    result.reserve(items.size());
    for (auto const& item : items) {
        result.push_back(item->operation());
    }
    return result;
}

std::unique_ptr<AbstractMatrixFileProcessor> MatrixOperations::create(const std::string &operation) {
    using namespace std;

    auto lower_operation(operation);
    transform(begin(lower_operation), end(lower_operation), begin(lower_operation),
        [](unsigned char c) { return std::tolower(c); });

    auto it = find_if(begin(items), end(items), [operation](IMatrixFileProcessorFactoryUPtr const& item) {
        return item->operation() == operation;
    });
    if (it != end(items)) {
        return (*it)->create();
    }

    throw MatrixFileProcessorError("Wrong operation: " + operation);
}
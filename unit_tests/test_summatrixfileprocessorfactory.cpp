#include "gtest/gtest.h"

#include "SumMatrixFileProcessorFactory.h"
#include "SumMatrixFileProcessor.h"


TEST(TestSumMatrixFileProcessorFactory, can_create_sum_op) {
    SumMatrixFileProcessorFactory fabric;
    auto op = fabric.create();

    auto rightType = dynamic_cast<SumMatrixFileProcessor*>(op.get());
    ASSERT_TRUE(rightType);
}

TEST(TestSumMatrixFileProcessorFactory, correct_name_of_operation) {
    SumMatrixFileProcessorFactory fabric;

    ASSERT_EQ(std::string("sum"), fabric.operation());
}

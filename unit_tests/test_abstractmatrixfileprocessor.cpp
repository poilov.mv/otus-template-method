#include "gtest/gtest.h"

#include "AbstractMatrixFileProcessor.h"
#include "TextFile.h"

struct FakeMatrixFileProcessor : public AbstractMatrixFileProcessor {
    bool applyOp_called = false;
    std::string applyOp_args;
    std::string applyOp_result;

    std::string applyOp(const std::string &content) override {
        applyOp_called = true;
        applyOp_args = content;
        return applyOp_result;
    }
};

class TestAbstractMatrixFileProcessor : public ::testing::Test {
protected:
    FakeMatrixFileProcessor processor;
    TextFile sourceFile;
    TextFile targetFile;

    TestAbstractMatrixFileProcessor()
        : sourceFile("source.txt")
        , targetFile("target.txt") {}

    void TearDown() {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestAbstractMatrixFileProcessor, executes_applyOp) {
    sourceFile.write();
    processor.apply(sourceFile.path(), targetFile.path());

    ASSERT_TRUE(processor.applyOp_called);
}

TEST_F(TestAbstractMatrixFileProcessor, executes_read_right_xontent) {
    sourceFile.setContent("1 4 7 2 5 8 3 6 9");
    sourceFile.write();
    processor.apply(sourceFile.path(), targetFile.path());

    ASSERT_TRUE(processor.applyOp_called);
    ASSERT_EQ("1 4 7 2 5 8 3 6 9", processor.applyOp_args);
}

TEST_F(TestAbstractMatrixFileProcessor, executes_write_right_content) {
    sourceFile.write();
    processor.applyOp_result = "some result";

    processor.apply(sourceFile.path(), targetFile.path());

    targetFile.read();
    ASSERT_EQ("some result", targetFile.getContent());
}

TEST_F(TestAbstractMatrixFileProcessor, exception_contains_expected_text) {
    try {
        throw MatrixFileProcessorError("some text");
    } catch (const MatrixFileProcessorError&ex) {
        ASSERT_EQ(std::string("some text"), std::string(ex.what()));
    }
}

TEST_F(TestAbstractMatrixFileProcessor, raise_error_if_source_file_not_exists) {
    ASSERT_THROW({
        processor.apply(sourceFile.path(), targetFile.path());
        }, MatrixFileProcessorError);
}

TEST_F(TestAbstractMatrixFileProcessor, raise_error_if_can_not_save_to_target) {
    sourceFile.setContent("1 4 7 2 5 8 3 6 9");
    sourceFile.write();

    ASSERT_THROW({
        processor.apply(sourceFile.path(), "realy/wrong?*.name");
        }, MatrixFileProcessorError);
}

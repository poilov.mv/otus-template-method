#include "gtest/gtest.h"

#include "TransposeMatrixFileProcessorFactory.h"
#include "TransposeMatrixFileProcessor.h"


TEST(TestTransposeMatrixFileProcessorFactory, can_create_transpose_op) {
    TransposeMatrixFileProcessorFactory fabric;
    auto op = fabric.create();

    auto rightType = dynamic_cast<TransposeMatrixFileProcessor*>(op.get());
    ASSERT_TRUE(rightType);
}

TEST(TestTransposeMatrixFileProcessorFactory, correct_name_of_operation) {
    TransposeMatrixFileProcessorFactory fabric;

    ASSERT_EQ(std::string("transpose"), fabric.operation());
}

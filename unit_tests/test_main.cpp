#include "gtest/gtest.h"

#include "main_app.h"
#include "TextFile.h"


class TestMain : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;

    TestMain()
        : sourceFile("source.txt")
        , targetFile("target.txt") {
    }

    void TearDown() override {
        sourceFile.remove();
        targetFile.remove();
    }

    int run_main(
        const std::string &operation = std::string(),
        const std::string &sourceFilePath = std::string(),
        const std::string &targetFilePath = std::string()
        ) {
        std::string pr("program");
        std::string op(operation);
        std::string sp(sourceFilePath);
        std::string tp(targetFilePath);

        int count = 1
            + (!op.empty() ? 1 : 0)
            + (!sp.empty() ? 1 : 0)
            + (!tp.empty() ? 1 : 0);

        char *argv[] = { &pr.at(0), 
            op.empty() ? NULL : &op.at(0),
            sp.empty() ? NULL : &sp.at(0), 
            tp.empty() ? NULL : &tp.at(0) };
        return main_app(count, argv);
    }
};


TEST_F(TestMain, can_transpose_matrix_from_source_to_target_file) {
    sourceFile.setContent("10 20 30 40 50 60 70 80 90");
    sourceFile.write();

    ASSERT_EQ(0, run_main("transpose", sourceFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "10 40 70 20 50 80 30 60 90";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, can_sum_matricies_from_source_to_target_file) {
    sourceFile.setContent("10 20 30 40 50 60 70 80 90\n1 2 3 4 5 6 7 8 9");
    sourceFile.write();

    ASSERT_EQ(0, run_main("sum", sourceFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "11 22 33 44 55 66 77 88 99";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, can_determinant_matrix_from_source_to_target_file) {
    sourceFile.setContent("10 20 30 40 50 60 70 80 90");
    sourceFile.write();

    ASSERT_EQ(0, run_main("determinant", sourceFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "0";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, show_synopsis_and_returns_error_without_args) {
    sourceFile.write();

    ASSERT_EQ(1, run_main());
}

TEST_F(TestMain, returns_error_with_wrong_source_file) {
    ASSERT_EQ(2, run_main("transpose", sourceFile.path(), targetFile.path()));
}

TEST_F(TestMain, returns_error_with_wrong_target_file) {
    sourceFile.setContent("10 20 30 40 50 60 70 80 90");
    sourceFile.write();

    auto targetPath = "wrong_file_name_?*/.txt";
    ASSERT_EQ(2, run_main("transpose", sourceFile.path(), targetPath));
}

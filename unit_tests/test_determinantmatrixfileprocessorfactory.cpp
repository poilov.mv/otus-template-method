#include "gtest/gtest.h"

#include "DeterminantMatrixFileProcessorFactory.h"
#include "DeterminantMatrixFileProcessor.h"


TEST(TestDeterminantMatrixFileProcessorFactory, can_create_determinant_op) {
    DeterminantMatrixFileProcessorFactory fabric;
    auto op = fabric.create();

    auto rightType = dynamic_cast<DeterminantMatrixFileProcessor*>(op.get());
    ASSERT_TRUE(rightType);
}

TEST(TestDeterminantMatrixFileProcessorFactory, correct_name_of_operation) {
    DeterminantMatrixFileProcessorFactory fabric;

    ASSERT_EQ(std::string("determinant"), fabric.operation());
}

#include "gtest/gtest.h"

#include "Matrix3.h"

#include <fstream>
#include <sstream>


TEST(TestMatrix3, can_create_matrix_from_vector) {
    std::vector<double> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto m = Matrix3::fromVector(v);

    ASSERT_EQ(v, m.toVector());
}

TEST(TestMatrix3, can_access_to_any_item_in_empty_matrix) {
    Matrix3 m;

    ASSERT_NO_THROW({
            for (int i = 0; i<Matrix3::ROWS; ++i) {
                for (int j = 0; j<Matrix3::COLS; ++j) {
                    m.at(j, i);
                }
            }
        });
}

TEST(TestMatrix3, can_transpose_matrix) {
    std::vector<double> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto m = Matrix3::fromVector(v);

    m = m.transposed();

    std::vector<double> v_transposed{ 1, 4, 7, 2, 5, 8, 3, 6, 9 };
    auto m_transposed = Matrix3::fromVector(v_transposed);

    ASSERT_EQ(m_transposed.toVector(), m.toVector());
}

TEST(TestMatrix3, can_sum_matricies) {
    std::vector<double> v1{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto m1 = Matrix3::fromVector(v1);

    std::vector<double> v2{ 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    auto m2 = Matrix3::fromVector(v2);

    auto m = m1 + m2;

    std::vector<double> v_sum{ 10, 10, 10, 10, 10, 10, 10, 10, 10 };
    auto m_sum = Matrix3::fromVector(v_sum);

    ASSERT_EQ(m_sum.toVector(), m.toVector());
}

TEST(TestMatrix3, can_get_determinant) {
    std::vector<double> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto m = Matrix3::fromVector(v);

    auto det = m.det();

    ASSERT_EQ(0, det);
}

TEST(TestMatrix3, can_get_determinant_one) {
    std::vector<double> v{ 1, 0, 8, 3, 2, 5, 1, 7, 2 };
    auto m = Matrix3::fromVector(v);

    auto det = m.det();

    ASSERT_EQ(121, det);
}

TEST(TestMatrix3, can_get_determinant_filled_with_ones) {
    std::vector<double> v{ 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    auto m = Matrix3::fromVector(v);

    auto det = m.det();

    ASSERT_EQ(0, det);
}

TEST(TestMatrix3, can_get_determinant_ident) {
    std::vector<double> v{ 1, 0, 0, 0, 1, 0, 0, 0, 1 };
    auto m = Matrix3::fromVector(v);

    auto det = m.det();

    ASSERT_EQ(1, det);
}

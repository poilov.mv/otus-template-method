#include "gtest/gtest.h"

#include "TextFile.h"

#include <fstream>
#include <sstream>


class TestTextFile : public ::testing::Test {
protected:
    TextFile sourceFile;

    TestTextFile()
        : sourceFile("source.txt") {}

    void SetUp() override {
    }

    void TearDown() override {
        sourceFile.remove();
    }
};


TEST_F(TestTextFile, can_get_file_path) {
    ASSERT_EQ("file.txt", TextFile("file.txt").path());
}

TEST_F(TestTextFile, cant_read_unexisting_file) {
    TextFile file("unexisting file.txt");

    ASSERT_FALSE(file.read());
}

TEST_F(TestTextFile, can_check_file_not_exists) {
    TextFile file("unexisting file.txt");

    ASSERT_FALSE(file.exists());
}

TEST_F(TestTextFile, can_check_file_exists) {
    sourceFile.write();

    ASSERT_TRUE(sourceFile.exists());
}

TEST_F(TestTextFile, cant_write_file_with_wrong_name) {
    TextFile file("unexisting file/?*.txt");

    ASSERT_FALSE(file.write());
}

TEST_F(TestTextFile, cant_remove_file) {
    sourceFile.write();

    sourceFile.remove();

    ASSERT_FALSE(sourceFile.exists());
}

TEST_F(TestTextFile, dont_write_file_without_write_called) {
    sourceFile.setContent("some text");

    ASSERT_FALSE(sourceFile.exists());
}

TEST_F(TestTextFile, can_write_file_with_content) {
    sourceFile.setContent("some text");
    sourceFile.write();

    ASSERT_TRUE(sourceFile.exists());
}

TEST_F(TestTextFile, dont_read_file_without_read_called) {
    sourceFile.setContent("some text");
    sourceFile.write();

    TextFile reader(sourceFile.path());

    ASSERT_NE(sourceFile.getContent(), reader.getContent());
    ASSERT_TRUE(reader.getContent().empty());
}

TEST_F(TestTextFile, can_read_file_with_right_content) {
    sourceFile.setContent("some text");
    sourceFile.write();

    TextFile reader(sourceFile.path());
    reader.read();

    ASSERT_EQ(sourceFile.getContent(), reader.getContent());
}
